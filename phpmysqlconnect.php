<?php
require_once 'dbconfig.php';

/*
CREATE TABLE IF NOT EXISTS users (
    id     INT AUTO_INCREMENT PRIMARY KEY,
    directory     VARCHAR (255)        DEFAULT NULL,
	username     VARCHAR (255)        DEFAULT NULL,
	password     VARCHAR (500)        DEFAULT NULL,
);
*/

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    echo "Connected to $dbname at $host successfully.";
} catch (PDOException $pe) {
    die("Could not connect to the database $dbname :" . $pe->getMessage());
}


$pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

$sql = 'SELECT username, password, directory 
        FROM users
        ORDER BY username';

$q = $pdo->query($sql);

$q->setFetchMode(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html>
<head>
<title>File Manager</title>
</head>
<body>
	
<table class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Job Title</th>
        </tr>
    </thead>
    <tbody>
        <?php while ($r = $q->fetch()): ?>
            <tr>
                <td><?php echo htmlspecialchars($r['username']) ?></td>
                <td><?php echo htmlspecialchars($r['password']); ?></td>
                <td><?php echo htmlspecialchars($r['directory']); ?></td>
            </tr>
        <?php endwhile; ?>
    </tbody>
</table>

</body>
</html>
